


    joystick = love.joystick.getJoysticks()[1]
    require("src/RealGamepad/gamepadinput")
    loadcontrollermappings()
    gamepadTimer = 0

	leftxaxis,leftyaxis=0,0
	
	require("NoobHub/main")	-- load NoobHub Client
	require("chatHub/chatHub")	-- chat hud based on noobHUB
	noobHubEnabled = true
	chathudFlag = false
	
	NetworkConnected=false	-- this variable is used to triggers so event when an instance connect to another (such as sounds or gui notifications)
	skinEnabled=false
	NetworkMenuVisible=false
	LastOrientation="portrait"
	MobileOrientation="portrait"
alesianaFont = love.graphics.newFont("src/assets/fonts/alanesiana/AlanesianaRegular.ttf", 24) -- Thanks @hicchicc for the font
alesianaFontBig = love.graphics.newFont("src/assets/fonts/alanesiana/AlanesianaRegular.ttf", 64) -- Thanks @hicchicc for the font

local touchControls = require("src/touchControls")

local touchKeyboard = require ("src/touchKeyboard")

require ("src/assets/loadVoices") loadVoices()

require ("src/Menu/About")

require ("src/Menu/Buttons")
require ("src/Menu/DrawButton")

--require ('TopMenu')




require ("src/Network/autobind")
require ("src/Network/receiveButtons")
require ("src/Network/sendButtons")
require ("src/Network/receiveThumbstickAxes")
require ("src/Network/sendAxes")
require ("src/Network/triggerConnectionEvent")
require ("src/Menu/NetworkSetup")

-- Define a coroutine for listening to messages
local messageCoroutine

-- input fields from https://github.com/ReFreezed/InputField

--require ("src/multipleFields/main")

function love.load()
	--love.window.setMode(1080, 2340, {resizable=true, borderless=false})
 -- Start the coroutine
    messageCoroutine = coroutine.create(listenForMessages)
    
  touchControls:init()
   --touchKeyboard:init() -- Initialize touchKeyboard module
end

function assignRealGamepadAxisToVirtualAxis()
    -- Define the threshold for assigning values
    local threshold = 5

    -- Define the range for the real gamepad's axis values (-1 to 1)
    local minAxisValue = -1
    local maxAxisValue = 1

    -- Define the range for the virtual axes (-40 to 40)
    local minVirtualValue = -40
    local maxVirtualValue = 40

    -- Map the axis values from the real gamepad to the virtual axes
    local mappedLeftXAxis = map(leftxaxis, minAxisValue, maxAxisValue, minVirtualValue, maxVirtualValue)
    local mappedLeftYAxis = map(leftyaxis, minAxisValue, maxAxisValue, minVirtualValue, maxVirtualValue)
    local mappedRightXAxis = map(rightxaxis, minAxisValue, maxAxisValue, minVirtualValue, maxVirtualValue)
    local mappedRightYAxis = map(rightyaxis, minAxisValue, maxAxisValue, minVirtualValue, maxVirtualValue)

    -- Only update touch control values if real gamepad's thumbsticks are actively used
    if math.abs(mappedLeftXAxis) > threshold or math.abs(mappedLeftYAxis) > threshold or
       math.abs(mappedRightXAxis) > threshold or math.abs(mappedRightYAxis) > threshold then
        touchControls.leftxaxis = mappedLeftXAxis
        touchControls.leftyaxis = mappedLeftYAxis
        touchControls.rightxaxis = mappedRightXAxis
        touchControls.rightyaxis = mappedRightYAxis
    end
end




-- Helper function to map a value from one range to another
function map(value, inMin, inMax, outMin, outMax)
    return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin
end

function love.keypressed(key, scancode, isRepeat)
	if NetworkMenuVisible==true then 
		loveInputFieldskeypressed(key, scancode, isRepeat)		-- listen to keyboard input for the network setup form
	elseif NetworkMenuVisible==false then
		switchInstances(key, scancode, isRepeat)	-- check key 1 and 2 to switch port and instance number
	end
	
	    -- Detect movement keys
    local input = {}

    if love.keyboard.isDown("w") then input["up"] = true end
    if love.keyboard.isDown("s") then input["down"] = true end
    if love.keyboard.isDown("a") then input["left"] = true end
    if love.keyboard.isDown("d") then input["right"] = true end

    -- If any key is pressed, send data
    if next(input) then
        sendGameData({ input = input })
    end
end

function switchInstances(key, scancode, isRepeat)	--not used for now
-- If the key pressed is "1", set instanceNumber to 1
    if key == "1" then
        instanceNumber = 1
        	receivePort=1024
			sendPort=1025
        print("Instance selected: 1")
    end

    -- If the key pressed is "2", set instanceNumber to 2
    if key == "2" then
        instanceNumber = 2
			receivePort=1025
			sendPort=1024
        print("Instance selected: 2")
    end
end

function love.update(dt)


updateButtonHeld(dt)
--checkOrientation()
	if menuOpened==false then
		switchInstances()	-- check for key 1 and 2 to switch instances and ports
	end
	--print(receiveAddress)
    updateKeyboard()
    
    --real gamepad
    updategetjoystickaxis(dt)
    
  isjoystickbeingpressed(joystick, button)
	
    if joystick then
        assignRealGamepadAxisToVirtualAxis()    -- assign real Gamepad values to virtual gamepad
        --releaseJoystickButtons(joystick, button) -- Release joystick buttons
    end
	
	-- network controls
	--receiveThumbstickAxes()
	--receiveButtons()

	
	--sendThumbstickAxes(touchControls.leftxaxis, touchControls.leftyaxis, touchControls.rightxaxis, touchControls.rightyaxis)
	--sendButtons(btnIndex, btnPressed)
	
	if noobHubEnabled == true then
		NoobHubClientUpdate(dt)			-- online multiplayer based on NoobHub (https://github.com/Overtorment/NoobHub)
	end
	
	
	if chathudFlag == true then
		chathudUpdate(dt)	-- this update a function that check gptCoroutine, a coroutine that works on a second thread to keep the game running 
	end						-- while waiting for a GPT4ALL response

  
    if NetworkMenuVisible==true then
		loveInputFieldUpdate(dt)
		  -- Update the coroutine
			coroutine.resume(messageCoroutine)	-- listen for messages for autobind
	end

	if searchingForInstances==true then
			testIPRange(ipRangeStart, ipRangeEnd, receivePort)
    end
   
end



function love.draw()
--print(receiveAddress)
		--if MobileOrientation == "portrait" then
			--love.graphics.scale( 1 )
			love.graphics.rotate( 0 )
			love.graphics.translate(0,0)
	--[[elseif MobileOrientation == "landscape" then
			--love.graphics.scale( 1 )
			love.graphics.rotate( 1.57 )
			love.graphics.translate(200,-1000)
	end--]]

  if menuOpened==false then
		touchControls:draw()
		touchControls:draw2()
  
	if touchControls.layout=="keyboard" then
		touchKeyboard:draw()
	end
	love.graphics.setFont(alesianaFont)
	-- debug statements
	love.graphics.setColor(0,0,1,1)
	love.graphics.print("Instance: " .. instanceNumber, touchControls.leftThumbstickX + 0,touchControls.leftThumbstickY-550)
	love.graphics.setColor(0,1,1,1)
	love.graphics.print("Listening on: " .. receiveAddress .. " port:" .. receivePort, touchControls.leftThumbstickX + 0,touchControls.leftThumbstickY-500)
	love.graphics.setColor(1,1,0,1)
	
	love.graphics.print("sending on: " .. sendAddress .. " port:" .. sendPort, touchControls.leftThumbstickX + 0,touchControls.leftThumbstickY-450)
	love.graphics.setColor(1,0,1,1)
				
	love.graphics.print("Received Left X Axis:" .. touchControls.receivedLeftXAxis,touchControls.leftThumbstickX + 1000,touchControls.leftThumbstickY-250)
	love.graphics.print("Received Left Y Axis:" .. touchControls.receivedLeftYAxis,touchControls.leftThumbstickX + 1000,touchControls.leftThumbstickY-300)
	
	-- Print values on the game window
	love.graphics.setColor(0,1,0,1)
	love.graphics.print("Left Thumbstick - x: " .. touchControls.leftxaxis .. " y: " .. touchControls.leftyaxis, touchControls.leftThumbstickX + 0,touchControls.leftThumbstickY-250)
	--love.graphics.setColor(0,0,1,1)
	--love.graphics.print("Right Thumbstick - x: " .. touchControls.rightxaxis .. " y: " .. touchControls.rightyaxis, 10, 50)
		if NetworkConnected==true then NetworkConnectedString = "True"
	elseif NetworkConnected==false then NetworkConnectedString = "False"
	end
	love.graphics.print("Network connected: " .. NetworkConnectedString, touchControls.leftThumbstickX + 0,touchControls.leftThumbstickY-600)

	
	 
	 if NetworkConnected==true then
		love.graphics.setColor(1,0,0,1)
		love.graphics.print("Connected to " .. connectedInstance,10,220) 
		love.graphics.setColor(1,1,1,1)
				if  connectedInstance == "Boxclip_Game" then
					love.graphics.draw(boxclipplayer,love.graphics.getWidth() / 2,love.graphics.getHeight() / 2,0,1,1)
			elseif   connectedInstance == "Starphase_Game" then
					love.graphics.draw(starphaseShip,love.graphics.getWidth() / 2,love.graphics.getHeight() / 2,0,0.3,0.3)
			end
		end
	 
		love.graphics.setFont(alesianaFont)
		--drawOpenMenu()
		
		
	elseif menuOpened==true then
	
	end
	
	if NetworkMenuVisible==false then
			--if showAbout==false then drawNetworkSetup() end
				--drawMenuButtons()
				
	elseif NetworkMenuVisible==true then
				loveInputFieldDraw()
				
				--drawAutobindButton()
				drawCloseNetworkSetup()
				
	end
	
	
	if showAbout==true then
		
		love.graphics.print("About: ",10, 10,0,1)
		drawTextContent(paragraph, windowX, windowY, maxWidth, lineHeight)
	end
	
	if skinEnabled==false then
	if joystick then
	    if leftxaxis>0.2 or leftxaxis<-0.2 or leftyaxis>0.2 or leftyaxis<-0.2  then 
	    -- Draw virtual thumbstick for left thumbstick when pressed and dragged
				love.graphics.circle("line", touchControls.leftThumbstickX + touchControls.leftxaxis, touchControls.leftThumbstickY + touchControls.leftyaxis, touchControls.leftThumbstickRadius * 1.5)
	    end
	    
	     if rightxaxis>0.2 or rightxaxis<-0.2 or rightyaxis>0.2 or rightyaxis<-0.2 and skinEnabled==false  then 
	    -- Draw virtual thumbstick for right thumbstick when pressed and dragged
				love.graphics.circle("line", touchControls.rightThumbstickX + touchControls.rightxaxis, touchControls.rightThumbstickY + touchControls.rightyaxis, touchControls.rightThumbstickRadius * 1.5)
	    end
	 end
	end 
	 
	 if searchingForInstances==true then
			love.graphics.print("Autobind pressed, please wait, searching in process", love.graphics.getWidth() /2,love.graphics.getHeight()/2, 0,1)
	 end
	 
	 if menuVisible==true then
			revealMenu(x, y, button, istouch, presses)
		end
		    -- Serialize thumbstick axes
    local data = string.format("%f,%f,%f,%f", leftxaxis, leftyaxis, rightxaxis, rightyaxis)

		 local text = "Data to be sent: " .. data .. " to " .. sendAddress .. " on port " .. sendPort
    love.graphics.print(text, touchControls.leftThumbstickY - 700, 100)

	if chathudFlag == true then
		chathuddraw()
	end

end

--[[
function checkOrientation()
	local desktopWidth, desktopHeight = love.window.getDesktopDimensions()
	--print(desktopWidth)
	--MobileOrientation="landscape"
		if desktopWidth<1100 then MobileOrientation="portrait"
			if LastOrientation==MobileOrientation then 


			else	
				LastOrientation=MobileOrientation
				scale_x = love.graphics.getWidth() / 2
				scale_y = love.graphics.getHeight() / 2
				love.window.setMode(2340, 1080, {resizable=true, borderless=false})
				touchControls:createArcadeButtons()
			end
	elseif desktopWidth>1100  then MobileOrientation="landscape"
			if LastOrientation==MobileOrientation then 

			else	
				LastOrientation=MobileOrientation
				scale_x = love.graphics.getWidth() / 2
				scale_y = love.graphics.getHeight() / 2
				love.window.setMode(1080, 2340, {resizable=true, borderless=false})
				touchControls:createArcadeButtons()
			end
	end
end
--]]
