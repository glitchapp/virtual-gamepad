
	EnableRemoteButton = {
	text = "Enable remote control",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4+500,
    sx = 3,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	
	NetworkSetupButton = {
	text = "Network setup",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4+400,
    sx = 3,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	
	autobindButton = {
	text = "autobind",
	textPosition ="top",
	x = 100, y = love.graphics.getHeight()/1.4,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	
		CloseNetworkButton = {
	text = "close network",
	textPosition ="top",
	x = love.graphics.getWidth() /1.3, y = 6,
    sx = 1,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	
function drawNetworkSetup()
    -- Network Setup
    hovered = isButtonHovered(NetworkSetupButton)
    drawButton(NetworkSetupButton, hovered, NetworkSetupButton.text)

    if hovered and love.mouse.isDown(1) and not NetworkMenuVisible then
        --loadRemoteControls()
        NetworkMenuVisible = true
        require("src/multipleFields/main")
        love.timer.sleep(0.3)
    elseif hovered then
        love.graphics.print("Network setup", love.graphics.getWidth() / 3, love.graphics.getHeight() - 50, 0, 1)
    end
    
end

function EnableRemoteControls()
    -- Enable remote controls
    hovered = isButtonHovered(EnableRemoteButton)
    drawButton(EnableRemoteButton, hovered, EnableRemoteButton.text)
    
    if hovered and love.mouse.isDown(1) then
		
        if enableRemoteControls==true then 
            enableRemoteControls = false
        elseif enableRemoteControls==false then 
            enableRemoteControls = true
            loadRemoteControls()
        end
        
        love.timer.sleep(0.3)
    elseif hovered then
        if enableRemoteControls then 
            love.graphics.print("Enable remote controls: Enabled", love.graphics.getWidth() / 3, love.graphics.getHeight() - 50, 0, 1)
        else
            love.graphics.print("Enable remote controls: Disabled", love.graphics.getWidth() / 3, love.graphics.getHeight() - 50, 0, 1)
        end
    end
end

function drawCloseNetworkSetup()
--Close Network Button
	hovered = isButtonHovered (CloseNetworkButton)
	drawButton (CloseNetworkButton, hovered,CloseNetworkButton.text)

	if hovered and love.mouse.isDown(1) then 
	
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			
			NetworkMenuVisible=false
			menuOpened=false showAbout=false
			touchControls.layout = "gamepad"
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
			love.graphics.print("Close Network setup", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end


end

function drawAutobindButton()	
	--Autobind Button
	hovered = isButtonHovered (autobindButton)
	drawButton (autobindButton, hovered,autobindButton.text)

	if hovered and love.mouse.isDown(1) then 
	
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			searchingForInstances=true
			love.graphics.print("Autobind pressed, please wait, searching in process", love.graphics.getWidth() /2,love.graphics.getHeight()/2, 0,1)
			--searchingForValidInstances:play()
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
			love.graphics.print("Auto bind other instances within the range provided", 50,love.graphics.getHeight()-120, 0,1)
			love.graphics.print("This feature does not work yet and is here", 90,love.graphics.getHeight()-90, 0,1)
			love.graphics.print("for further testing and development purposes", 90,love.graphics.getHeight()-60, 0,1)
	end
	
end
