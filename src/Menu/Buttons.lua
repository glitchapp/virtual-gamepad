	--[[OpenMenuButton = {
	text = "Menu",
	textPosition ="top",
	x = love.graphics.getWidth() -450, y = 50,
	--x = 100, y = 100,
    sx = 3,
    --image = GamepadLayoutImg,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	--]]
	
	QuickSwitchLayoutButton = {
	text = "QuickSwitch",
	textPosition ="top",
	x = love.graphics.getWidth() /1.8, y = 5,
    sx = 3,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	
	GamepadLayoutButton = {
	text = "Gamepad layout",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4,
    sx = 3,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	
	ArcadeLayoutButton = {
	text = "Arcade layout",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4+100,
    sx = 3,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	
	KeyboardLayoutButton = {
	text = "Keyboard layout",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4+200,
    sx = 3,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	
	AboutButton = {
	text = "About",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = love.graphics.getHeight() / 4+500,
    sx = 3,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	
	CloseAboutButton = {
	text = "close About",
	textPosition ="top",
	x = love.graphics.getWidth() /1.3, y = 150,
    sx = 3,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}
	
	PerformanceButton = {
	text = "Performance mode",
	textPosition ="top",
	x = love.graphics.getWidth() / 4, y = 50,  y = love.graphics.getHeight() / 4+300,
    sx = 3,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = large,
    unlocked = true,
	}
	
	SoundOnOffButton = {
	text = "Sound",
	textPosition ="top",
	x = love.graphics.getWidth() -250, y = 50,  y = 50,
    sx = 3,
    hovered = false,
    color = {1, 1, 1},
    hoveredColor = {1, 1, 0},
    font = menu,
    unlocked = true,
	}


menuOpened=false
showAbout=false

function drawOpenMenu()

--[[-- Open menu button
	hovered = isButtonHovered (SoundOnOffButton)
	drawButton (SoundOnOffButton, hovered,SoundOnOffButton.text)

	if hovered and love.mouse.isDown(1) then 
	--inputtime=0
		showAbout=false
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) or touchControls:checkButtonPress(x, y) then
					if soundIsOn==true then soundIsOn=false
				elseif soundIsOn==false then soundIsOn=true
				end
				 love.timer.sleep( 0.3 )
				 return
		end
	elseif hovered then
				love.graphics.print("Settings menu", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
--]]
	if showAbout==false then
-- Open menu button
	hovered = isButtonHovered (OpenMenuButton)
	drawButton (OpenMenuButton, hovered,OpenMenuButton.text)

	if hovered and love.mouse.isDown(1) then 
	--inputtime=0
		showAbout=false
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) or touchControls:checkButtonPress(x, y) then
				 if menuOpened==true then menuOpened=false
				 love.timer.sleep( 0.3 )
				 return
			 elseif menuOpened==false then menuOpened=true
			love.timer.sleep( 0.3 )
			 return
			
			end
		end
	elseif hovered then
				love.graphics.print("Settings menu", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
	

	end
	
end

function drawMenuButtons()


if menuOpened==true and showAbout==false then

--[[	--gamepad Layout Button
	hovered = isButtonHovered (GamepadLayoutButton)
	drawButton (GamepadLayoutButton, hovered,GamepadLayoutButton.text)

	if hovered and (love.mouse.isDown(1) or touchControls:checkButtonPress(x, y)) then 
	--inputtime=0
		
			  touchControls.layout = "gamepad"
			 touchControls:init()
			 menuOpened=false
			 showAbout=false
			love.timer.sleep( 0.3 )
			return
		
	elseif hovered then
				love.graphics.print("Gamepad layout", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
--]]
--Arcade Layout Button
	hovered = isButtonHovered (ArcadeLayoutButton)
	drawButton (ArcadeLayoutButton, hovered,ArcadeLayoutButton.text)

	if hovered and love.mouse.isDown(1) then 
	--inputtime=0
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			  touchControls.layout = "arcade"
			 touchControls:init()
			 menuOpened=false
			 showAbout=false
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
				love.graphics.print("Arcade layout", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
	
	--Keyboard Layout Button
	hovered = isButtonHovered (KeyboardLayoutButton)
	drawButton (KeyboardLayoutButton, hovered,KeyboardLayoutButton.text)

	if hovered and love.mouse.isDown(1) then 
	--inputtime=0
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			  touchControls.layout = "keyboard"
			 touchControls.buttons = {}
			 touchControls:init()
			 menuOpened=false
			 showAbout=false
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
				love.graphics.print("Keyboard layout", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
	--[[
	--Toggle performance mode
	hovered = isButtonHovered (PerformanceButton)
	drawButton (PerformanceButton, hovered,PerformanceButton.text)
		if performanceMode==true then	love.graphics.setColor(0,1,0,1) love.graphics.print("enabled",PerformanceButton.x+50,PerformanceButton.y,0,1)
	elseif performanceMode==false then	love.graphics.setColor(1,0,0,1) love.graphics.print("disabled",PerformanceButton.x+50,PerformanceButton.y,0,1)
	end
	love.graphics.setColor(1,1,1,1)

	if hovered and love.mouse.isDown(1) then 
	
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
				if performanceMode==false then performanceMode=true
			elseif performanceMode==false then	performanceMode=true
			end
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
				love.graphics.print("This option toggles performance mode", love.graphics.getWidth() /3,love.graphics.getHeight()-100, 0,1)
				love.graphics.print("This is useful to increase performance (it reduces parallax complexity)", 100,love.graphics.getHeight()-50, 0,1)
	end
	--]]
--About Button
	hovered = isButtonHovered (AboutButton)
	drawButton (AboutButton, hovered,AboutButton.text)

	if hovered and love.mouse.isDown(1) then 
	--inputtime=0
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			showAbout=true
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
			love.graphics.print("About", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end
	
end
if showAbout==true then
--Close About Button
	hovered = isButtonHovered (CloseAboutButton)
	drawButton (CloseAboutButton, hovered,CloseAboutButton.text)

	if hovered and love.mouse.isDown(1) then 
	
		if love.mouse.isDown(1) or isjoystickbeingpressed(joystick,button) then
			showAbout=false
			love.timer.sleep( 0.3 )
			return
		end
	elseif hovered then
			love.graphics.print("Close about", love.graphics.getWidth() /3,love.graphics.getHeight()-50, 0,1)
	end

end


end
