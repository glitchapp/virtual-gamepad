paragraph = 			  "Lovepad & Touch controls <br>" ..
                          "Copyright (C) 2024 Glitchapp <br>" ..
                          "https://codeberg.org/glitchapp/virtual-gamepad <br>" ..
                          ". <br>" ..
                          "This program is free software: you can redistribute it and/or modify " ..
                          "it under the terms of the GNU General Public License as published by " ..
                          "the Free Software Foundation, either version 2 of the License, or " ..
                          "(at your option) any later version.<br>" ..
                          ". <br>" ..
                          "This program is distributed in the hope that it will be useful, " ..
                          "but WITHOUT ANY WARRANTY; without even the implied warranty of " ..
                          "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the " ..
                          "GNU General Public License for more details." ..
                          ". <br>"
                          

			--paragraphtext = string.gsub(paragraph, "<br>", "\n")
			paragraphtext = paragraph:gsub("<[^>]+>", "\n")



			--paragraphWhatsNewtext = string.gsub(paragraphWhatsNew, "<br>", "\n")
		
			--paragraphDonatetext = string.gsub(paragraphDonate, "<br>", "\n")

	-- Initialize the window and button positions
	windowX, windowY = 0, 250  -- Initial window position
	--offsetX, offsetY = 400, 25  -- Offset for mouse dragging

-- Define maxWidth
		maxWidth = 700
		lineHeight = love.graphics.getFont():getHeight()*5
		--love.graphics.setFont(mainMonoFont32)
		
function drawTextContent(paragraph, windowX, windowY, maxWidth, lineHeight)

    -- Replace <br> tags with newline characters
    paragraph = paragraph:gsub("<br>", "\n")
    
    local lines = {}
    local currentLine = ""

    -- Split paragraph into lines based on newline characters
    for line in paragraph:gmatch("[^\n]+") do
        local words = {}
        for word in line:gmatch("%S+") do
            table.insert(words, word)
        end

        local testLine = currentLine
        for _, word in ipairs(words) do
            local testLineWidth = love.graphics.getFont():getWidth(testLine .. word .. " ")
            if testLineWidth <= maxWidth then
                testLine = testLine .. word .. " "
            else
                table.insert(lines, testLine)
                testLine = word .. " "
            end
        end

        if testLine ~= "" then
            table.insert(lines, testLine)
        end
        currentLine = ""
    end

    local x, y = windowX + 50, windowY + 210
    for _, line in ipairs(lines) do
		love.graphics.setColor(1,1,1)
        love.graphics.print(line, x, y,0,3,3)
        y = y + lineHeight
    end
end
