function loadcontrollermappings()
	if love.filesystem.getInfo("lib/gamecontrollerdb.txt") then
		love.joystick.loadGamepadMappings("lib/gamecontrollerdb.txt")
	end
	lastgamepadtouchedlevel="none"	-- this allows the gamepad to vibrate in first init (check line 68 of game/mainfunctions/gamepad/gamepadRadialMenu.lua where the condition is checked)
	
	movementTimer = 0
	movementDelay = 0.1  -- Adjust this value to control movement speed (in seconds)

	axisMouseAcceleration=10		-- acceleration value if thumbstick is moved

	--loadGamePadFiles()
	--initialize axes
	leftxaxis,leftyaxis,rightxaxis,rightyaxis=0,0,0,0
	
	
end

	-- Initialize variables for the updateButtonHeld(dt) function
	local buttonAHeldTime = 0
	local vibrationDelay = 0.75 -- Delay in seconds before vibration starts
	local lastVibrationTime = -vibrationDelay
	local vibrationInterval = 0.5 -- Vibration interval in seconds
	
	--joysticks = love.joystick.getJoysticks()
	--joystick = joysticks[1]


function updateButtonHeld(dt)

    if joystick then
		if joystick:isGamepadDown("a") then
			
			buttonAHeldTime = buttonAHeldTime + dt

			-- Check if it's time to start vibrating
			if buttonAHeldTime >= vibrationDelay then
				-- Check if it's time to vibrate
				if love.timer.getTime() - lastVibrationTime >= vibrationInterval then
					joystick:setVibration(0.2, 0.2, 0.1)
					lastVibrationTime = love.timer.getTime()
				end
			end
		else
			buttonAHeldTime = 0
		end
	
		if buttonAHeldTime > 3 and (languagehaschanged or musichaschanged or skinhaschanged or optionsmenupressed) then
			print("Saving changes")
			savemygame()
			languagehaschanged = false
			musichaschanged = false
			skinhaschanged = false
			optionsmenupressed = false
			if gamestatus=="gameplusoptions" then palette=1 end
			love.timer.sleep(0.3)
		end
    end

end





-- Function to update joystick axis and control emulated mouse movement
function updategetjoystickaxis(dt)
	gamepadTimer=gamepadTimer+dt
	 -- Check if a joystick is connected
	 if joystick ~= nil then
          -- Get values of left joystick axes
        leftxaxis = joystick:getGamepadAxis("leftx")*3
        leftyaxis = joystick:getGamepadAxis("lefty")*3
          
          -- Get values of right joystick axes
        rightxaxis = joystick:getGamepadAxis("rightx")
        rightyaxis = joystick:getGamepadAxis("righty")
        
          -- Get values of triggers
        tleft = joystick:getGamepadAxis('triggerleft')
        tright = joystick:getGamepadAxis('triggerright')

          -- Check if joystick is moved enough to register
        if leftxaxis>0.2 or leftxaxis<-0.2 then 
		
					if leftxaxis>0.2  then -- do nothing
				elseif leftxaxis<-0.2  then -- do nothing
				else
				end
				
		end
		
		if leftyaxis>0.2 or leftyaxis<-0.2 then
		-- Update emulated mouse y-coordinate
			
					if leftyaxis>0.4 then -- do nothing
				elseif leftyaxis<-0.2 then -- do nothing
				end
		end
 					
 
			-- activate zoom if right trigger is pressed
			if tright>0.2 then
				--zoomtriggered=true
		elseif tright<0.2 then
				--zoomtriggered=false
		end
		--[[
		 local data = {
            leftxaxis = joystick:getGamepadAxis("leftx") * 3,
            leftyaxis = joystick:getGamepadAxis("lefty") * 3,
            rightxaxis = joystick:getGamepadAxis("rightx"),
            rightyaxis = joystick:getGamepadAxis("righty"),
            tleft = joystick:getGamepadAxis("triggerleft"),
            tright = joystick:getGamepadAxis("triggerright")
        }

        sendGameData(data) -- Send data over NoobHub
        --]]
   end
   
end




function isjoystickbeingpressed(joystick, button)

    -- Check if any touch buttons are pressed
    local touchButtonsPressed = touchControls:checkButtonPress()

    -- If touch buttons are pressed, don't modify their state
    if touchButtonsPressed then
        return
    end

    -- Define mappings between gamepad buttons and button indices
    local buttonMappings = {
        a = 6,
        b = 5,
        x = 7,
        y = 8,
        dpleft = 3,
        dpright = 1,
        dpup = 4,
        dpdown = 2
    }

    -- Iterate through the button mappings
    for buttonName, btnIndex in pairs(buttonMappings) do
        if joystick and joystick:isGamepadDown(buttonName) then
            -- Set the button state to pressed
            for i, btn in pairs(touchControls.buttons) do
                if i == btnIndex then
                    btn.pressed = true
                    if enableRemoteControls then
                        sendButtons(btnIndex, true)
                    end
                end
            end
        elseif joystick then
            -- Set the button state to not pressed only if it's not already pressed by touch
            for i, btn in pairs(touchControls.buttons) do
                if i == btnIndex and not btn.pressed then
                    btn.pressed = false
                     if enableRemoteControls then
                        sendButtons(btnIndex, false)
                    end
                end
            end
        end
    end

    -- Handle other button presses (if needed)
    if joystick and joystick:isGamepadDown("start") then
        -- Handle Start button being pressed
    end
end

function releaseJoystickButtons(joystick, button)
    -- Define mappings between gamepad buttons and button indices
    local buttonMappings = {
        a = 6,
        b = 5,
        x = 7,
        y = 8,
        dpleft = 3,
        dpright = 1,
        dpup = 4,
        dpdown = 2
    }

    -- Check if any joystick button is pressed
    local anyButtonPressed = false
    for buttonName, btnIndex in pairs(buttonMappings) do
        if joystick and joystick:isGamepadDown(buttonName) then
            anyButtonPressed = true
            break
        end
    end

    -- If no button is pressed, release the buttons
    if not anyButtonPressed then
        for i, btn in pairs(touchControls.buttons) do
            btn.pressed = false
            if enableRemoteControls then
                sendButtons(i, false)
            end
        end
    end
end





--[[
-- Callback function when a gamepad button is pressed
function PlayStateisjoystickbeingpressed(joystick,button)
  	if joystick and joystick:isGamepadDown("leftshoulder") and (not play.tilt) then
            aplay(sounds.leftFlipper)
            pinball:moveLeftFlippers()
            print("test")
  	
  	end
end
--]]

--[[
-- Callback function when a gamepad button is pressed
function love.gamepadpressed(joystick,button)
  	if joystick and joystick:isGamepadDown("a") then
	
				if (self.state:on("main")) then  self:menuAction()
			elseif (self.state:on("config")) then  self:menuAction()
			elseif (self.state:on("about")) then   about:forward()
            elseif (self.state:on("scores")) then  self.state:set("main")
			end
	end
end
--]]
