function loadSkins()
	
	skinBase = love.graphics.newImage("skins/skin1/skinBase.png")
	
	-- directional pad
	downDpad = love.graphics.newImage("skins/skin1/downDpad.png")
	leftDpad = love.graphics.newImage("skins/skin1/leftDpad.png")
	rightDpad = love.graphics.newImage("skins/skin1/rightDpad.png")
	upDpad = love.graphics.newImage("skins/skin1/upDpad.png")
	
	-- thumbsticks
	leftThumbstick = love.graphics.newImage("skins/skin1/leftThumbstick.png")
	leftThumbstickBase = love.graphics.newImage("skins/skin1/leftThumbstickBase.png")
	rightThumbstick = love.graphics.newImage("skins/skin1/rightThumbstick.png")
	rightThumbstickBase = love.graphics.newImage("skins/skin1/rightThumbstickBase.png")
	
	--buttons
	aButton = love.graphics.newImage("skins/skin1/a.png")
	bButton = love.graphics.newImage("skins/skin1/b.png")
	xButton = love.graphics.newImage("skins/skin1/x.png")
	yButton = love.graphics.newImage("skins/skin1/y.png")
	
end
