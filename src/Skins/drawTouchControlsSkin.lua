
-- Update the existing draw function to draw the right thumbstick and its virtual thumbstick
function touchControls:skindraw()
 love.graphics.setColor(1,1,1,1)
love.graphics.draw(skinBase, 0, 0, 0, 1)
   
  -- Draw buttons for left thumbstick
  for i, button in pairs(self.buttons) do
  
			if i==6 then love.graphics.setColor(1,0,0,1)
		elseif i==2 then love.graphics.setColor(1,0,0,1)
		
		elseif i==3 then love.graphics.setColor(0,0,1,1)
		elseif i==7 then love.graphics.setColor(0,0,1,1)
		
		elseif i==4 then love.graphics.setColor(0,1,0,1)
		elseif i==8 then love.graphics.setColor(0,1,0,1)
		
		elseif i==1 then love.graphics.setColor(0,1,1,1)
		elseif i==5 then love.graphics.setColor(0,1,1,1)
		
		else love.graphics.setColor(1,1,1,1)
		end
  
    if button.pressed then
		
			if i==2 then love.graphics.draw(downDpad, button.x-50, button.y-50, 0, 2)
		elseif i==6 then love.graphics.draw(aButton, button.x-50, button.y-50, 0, 2)
		
		elseif i==3 then love.graphics.draw(leftDpad, button.x-50, button.y-50, 0, 2)
		elseif i==7 then love.graphics.draw(xButton, button.x-50, button.y-50, 0, 2)
		
		elseif i==4 then love.graphics.draw(upDpad, button.x-50, button.y-50, 0, 2)
		elseif i==8 then love.graphics.draw(yButton, button.x-50, button.y-50, 0, 2)
		
		elseif i==1 then love.graphics.draw(rightDpad, button.x-50, button.y-50, 0, 2)
		elseif i==5 then love.graphics.draw(bButton, button.x-50, button.y-50, 0, 2)
		end
		love.graphics.circle("fill", button.x, button.y, self.buttonRadius)
    else
		
			if i==2 then love.graphics.draw(downDpad, button.x-50, button.y-50, 0, 2)
		elseif i==6 then love.graphics.draw(aButton, button.x-50, button.y-50, 0, 2)
		
		elseif i==3 then love.graphics.draw(leftDpad, button.x-50, button.y-50, 0, 2)
		elseif i==7 then love.graphics.draw(xButton, button.x-50, button.y-50, 0, 2)
		
		elseif i==4 then love.graphics.draw(upDpad, button.x-50, button.y-50, 0, 2)
		elseif i==8 then love.graphics.draw(yButton, button.x-50, button.y-50, 0, 2)
		
		elseif i==1 then love.graphics.draw(rightDpad, button.x-50, button.y-50, 0, 2)
		elseif i==5 then love.graphics.draw(bButton, button.x-50, button.y-50, 0, 2)
		end
    end
  end
  
   -- Draw left thumbstick
  love.graphics.draw(leftThumbstickBase, self.leftThumbstickX-80, self.leftThumbstickY-80, 0, 1)
	if not (self.rightThumbstickPressed) then
		love.graphics.draw(leftThumbstick, self.leftThumbstickX-50 + touchControls.leftxaxis, self.leftThumbstickY - 30 + touchControls.leftyaxis, 0, 1)
	end

  -- Draw virtual thumbstick for left thumbstick when pressed and dragged
  if self.leftThumbstickPressed then
    love.graphics.draw(leftThumbstick, self.leftThumbstickX-50 + touchControls.leftxaxis, self.leftThumbstickY - 30 + touchControls.leftyaxis, 0, 1)
  end
  
	if not (layout=="arcade") then
  -- Draw right thumbstick
  love.graphics.draw(rightThumbstickBase, self.rightThumbstickX-80, self.rightThumbstickY-80, 0, 1)
	if not (self.rightThumbstickPressed) then
		love.graphics.draw(rightThumbstick, self.rightThumbstickX - 50 + touchControls.rightxaxis, self.rightThumbstickY -30 + touchControls.rightyaxis, 0, 1)
	end
  -- Draw virtual thumbstick for right thumbstick when pressed and dragged
  if self.rightThumbstickPressed then
    love.graphics.draw(rightThumbstick, self.rightThumbstickX - 50 + touchControls.rightxaxis, self.rightThumbstickY -30 + touchControls.rightyaxis, 0, 1)
  end
  end
  
end
