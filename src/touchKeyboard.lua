-- touchKeyboard.lua

-- List of sound sources
local soundSources = {
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-001.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-002.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-003.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-004.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-005.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-006.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-007.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-008.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-009.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-010.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-011.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-012.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-013.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-014.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-015.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-016.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-017.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-018.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-019.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-020.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-021.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-022.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-023.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-024.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-025.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-026.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-027.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-028.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-029.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-030.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-031.wav", "static"),
    love.audio.newSource("src/assets/unicae_games_keyboard_soundpack_1/Single Keys/keypress-032.wav", "static")
}

-- Function to play a random key stroke
function playRandomeKeyStroke()
	if soundIsOn==true then
		local randomIndex = love.math.random(1, #soundSources)
    
		soundSources[randomIndex]:setVolume(0.1)
		soundSources[randomIndex]:play()
    end
end

touchKeyboard = {}

-- Define the keyboard layout
keyboardLayout = {
     {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0","del"},
    {"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P","ret"},
    {"A", "S", "D", "F", "G", "H", "J", "K", "L", ""},
    {"Z", "X", "C", "V", "B", "N", "M", "", "", ""},
    {"", "", "", "", "", "", "", "", "", ""}

}

-- Define the keyboard layout
keyboardLayoutLandscape = {
    {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0","del"},
    {"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P","ret"},
    {"A", "S", "D", "F", "G", "H", "J", "K", "L", ""},
    {"Z", "X", "C", "V", "B", "N", "M", "", "", ""},
    {"", "", "", "", "", "", "", "", "", ""}

}

KeyboardOrientation= "portrait"

-- Function to select the appropriate keyboard layout based on orientation
function selectKeyboardLayout()
    if KeyboardOrientation == "landscape" then
        return keyboardLayoutLandscape
    else
        return keyboardLayout
    end
end

-- Define the position and size of the keyboard
--keyboardX = 50
--keyboardY = 100
keyWidth = 60
keyHeight = 60
keyPadding = 10


-- Initialize the virtual keyboard
function touchKeyboard:init()
    self.selectedKey = nil
end


require ("src/Keyboard/keyboardUpdate")
require ("src/Keyboard/keyboardDraw")




function touchKeyboard:touchpressed(x, y)
    
    -- Apply the inverse rotation to the touch coordinates
    local rotatedX, rotatedY = rotatePoint(x - self.x, y - self.y, -self.rotation)
    --print(rotatedX)
    
    -- Iterate over the keys and check for press
    local layout = selectKeyboardLayout() -- Select the appropriate keyboard layout
    for i, row in ipairs(layout) do
        for j, key in ipairs(row) do
            local keyX = (j - 1) * (keyWidth + keyPadding)
            local keyY = (i - 1) * (keyHeight + keyPadding)
            
            -- Check if the rotated coordinates are within the key boundaries
            if rotatedX >= keyX and rotatedX <= keyX + keyWidth and
               rotatedY >= keyY and rotatedY <= keyY + keyHeight then
                print("Selected key:", key)
                self.selectedKey = key
                
                -- Add mobile vibration (if supported)
                love.system.vibrate(0.1)
                
                return
            end
        end
    end
    
    -- If no key is selected
    print("No key selected")
end




-- Function to rotate a point (x, y) around the origin by angle theta
function rotatePoint(x, y, theta)
    local cosTheta = math.cos(theta)
    local sinTheta = math.sin(theta)
    local rotatedX = x * cosTheta - y * sinTheta
    local rotatedY = x * sinTheta + y * cosTheta
    return rotatedX, rotatedY
end

function touchKeyboard:mousepressed(x, y, button, istouch, presses)
    --if button == 1 then -- Left mouse button
        self:touchpressed(x, y)
    --end
end

function touchKeyboard:mousereleased(x, y, button, istouch, presses)
    --if button == 1 then -- Left mouse button
        self:touchreleased()
    --end
end


-- Handle touchreleased event
function touchKeyboard:touchreleased()
    self.selectedKey = nil
end

-- Return the selected key
function touchKeyboard:getSelectedKey()
		return self.selectedKey
end

return touchKeyboard
