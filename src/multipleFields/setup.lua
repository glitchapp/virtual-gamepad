io.stdout:setvbuf("no")
io.stderr:setvbuf("no")

if love.getVersion() < 11 then
	local _clear    = love.graphics.clear
	local _setColor = love.graphics.setColor

	function love.graphics.clear(r, g, b, a)
		_clear((r and r*255), (g and g*255), (b and b*255), (a and a*255))
	end

	function love.graphics.setColor(r, g, b, a)
		_setColor(r*255, g*255, b*255, (a and a*255))
	end
end

package.preload.InputField = function()
--debug statements
--[[
    local mainDirectory = love.filesystem.getRealDirectory("main.lua")
    if mainDirectory then
        local inputFieldPath = mainDirectory .. "/src/multipleFields/InputField.lua"
        print("Attempting to load file from path:", inputFieldPath)
        local loadedFunc, err = loadfile(inputFieldPath)
        if not loadedFunc then
            print("Error loading file:", err)
        else
            print("File loaded successfully")
            loadedFunc()  -- Execute the loaded file
        end
    else
        print("Failed to get the real directory of main.lua")
    end
--]]

	  --return assert(loadfile(love.filesystem.getSource().."/src/multipleFields/InputField.lua"))()
end
