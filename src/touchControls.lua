-- touchControls.lua

require ("src/Menu/Buttons")
require ("src/Menu/DrawButton")

touchControls = {
  leftThumbstickRadius = 50,
  rightThumbstickRadius = 50,
  buttonRadius = 25,
  buttonDistance = 60,
  leftxaxis = 0,
  leftyaxis = 0,
  rightxaxis = 0,
  rightyaxis = 0,
  leftThumbstickPressed = false,
  rightThumbstickPressed = false,
  buttons = {},
  leftThumbstickX = love.graphics.getHeight() / 1.2,
  leftThumbstickY = love.graphics.getHeight() / 2
}


-- Enum for layout types
local LayoutType = {
	GAMEPAD = 1,
    ARCADE = 2,
    KEYBOARD = 3, -- Add KEYBOARD layout
}


function touchControls:init()
 --if not (touchControls.layout == "keyboard") then
  -- Create left thumbstick
  self.leftThumbstickX = love.graphics.getWidth() / 10
  self.leftThumbstickY = love.graphics.getHeight() / 1.4

  -- Create right thumbstick
  self.rightThumbstickX = love.graphics.getWidth() / 4.8
  self.rightThumbstickY = love.graphics.getHeight() / 1.22
--end

    self:createArcadeButtons()
 
	
end

--require ("src/Gamepad/initTouchControls")
require ("src/Gamepad/createGamepadButtons")
require ("src/Gamepad/createArcadeButtons")
require ("src/Gamepad/removeLeftThumbstickButtons")
require ("src/Gamepad/removeRightThumbstickButtons")
require ("src/Gamepad/toggleLayout")
require ("src/Gamepad/touchEvents")
require ("src/Gamepad/mouseEvents")
require ("src/Menu/checkMenuButtons")
require ("src/Menu/IsHovered")

require ("src/multipleFields/main")


require ("src/Network/initNetwork")
loadRemoteControls()




require ("src/Gamepad/updateThumbstickAxes")
require ("src/Gamepad/checkButtonPress")
require ("src/Gamepad/drawTouchControls")
require ("src/Gamepad/drawTouchControls2")
require ("src/Gamepad/adjustButtons")



-- Return touchControls table
return touchControls

