
-- Register mousepressed event for left and right thumbsticks
function love.mousepressed(x, y, button, istouch, presses)

-- Adjust coordinates based on the current orientation
    if MobileOrientation == "landscape" then
        x, y = love.graphics.inverseTransformPoint(x, y)
    end

if not (touchControls.leftThumbstickX==nil) then
  local distanceToLeftThumbstick = math.sqrt((x - touchControls.leftThumbstickX)^2 + (y - touchControls.leftThumbstickY)^2)
  --local distanceToRightThumbstick = math.sqrt((x - touchControls.rightThumbstickX)^2 + (y - touchControls.rightThumbstickY)^2)

  if distanceToLeftThumbstick <= touchControls.leftThumbstickRadius then
    touchControls.leftThumbstickPressed = true
    touchControls:updateLeftThumbstickAxes(x, y)
  --elseif distanceToRightThumbstick <= touchControls.rightThumbstickRadius and not (layout=="arcade") then
    --touchControls.rightThumbstickPressed = true
    --touchControls:updateRightThumbstickAxes(x, y)
  else
    touchControls:checkButtonPress(x, y)
    touchControls.leftThumbstickPressed = false
    --TopMenuMousepressed(x, y, button, istouch, presses)
    if NetworkMenuVisible==true then
		InputFieldmousepressed(mx, my, mbutton, pressCount)
	end
    if not (layout=="arcade") then
		--touchControls.rightThumbstickPressed = false
	end
    --touchControls:checkButtonPress(x, y)
  end

  checkMenuButtons(x, y, button, istouch, presses)
  
  
  
  
end
  
end


--[[

love.mousepressed = function(x, y, button, istouch, presses)
  local distanceToThumbstick = math.sqrt((x - self.leftThumbstickX)^2 + (y - self.leftThumbstickY)^2)

  if distanceToThumbstick <= self.leftThumbstickRadius then
    print("Mouse pressed - Inside thumbstick")
    self.leftThumbstickPressed = true
	self:updateLeftThumbstickAxes(x, y)
    self:updateThumbstickAxes(x, y)
  else
    print("Mouse pressed - Outside thumbstick")
    self.leftThumbstickPressed = true
	self:updateLeftThumbstickAxes(x, y)
    self:checkButtonPress(x, y)
  end
end
--]]

-- Register mousemoved event for left and right thumbsticks
function love.mousemoved(x, y, dx, dy, istouch)
-- Adjust coordinates based on the current orientation
    if MobileOrientation == "landscape" then
        x, y = love.graphics.inverseTransformPoint(x, y)
    end

 
  if touchControls.leftThumbstickPressed then
    touchControls:updateLeftThumbstickAxes(x, y)
   
  --elseif touchControls.rightThumbstickPressed and not (layout=="arcade") then
--    touchControls:updateRightThumbstickAxes(x, y)
  else
    -- Handle other mousemoved logic
    --TopMenuMousemoved(x, y, dx, dy, istouch)
    if NetworkMenuVisible==true then
		InputFieldmousemoved(mx, my, dx, dy)
	end
  end
end



function touchControls:handleTouchPressed(id, x, y)
  local distanceToThumbstick = math.sqrt((x - self.leftThumbstickX)^2 + (y - self.leftThumbstickY)^2)

  if distanceToThumbstick <= self.leftThumbstickRadius then
    self.thumbstickPressed = true
    self:updateThumbstickAxes(x, y)
  else
    self:checkButtonPress(x, y)
  end
end

function touchControls:handleMouseMoved(x, y, dx, dy, istouch)
  if self.thumbstickPressed or self.mouseJustPressed then
    print("Mouse moved - Thumbstick pressed")
    self:updateThumbstickAxes(x, y)
  else
    print("Mouse moved - Thumbstick not pressed")
  end
  self.mouseJustPressed = false
end


function touchControls:handleMouseReleased(x, y, button, istouch, presses)
  if not istouch then
    self.thumbstickPressed = false
    self.leftxaxis = 0
    self.leftyaxis = 0

    for _, button in pairs(self.buttons) do
      button.pressed = false
      button1or5Pressed=false
      button2or6Pressed=false
    end
  end
end



-- Register mousereleased event
function love.mousereleased(x, y, button, istouch, presses)
-- Adjust coordinates based on the current orientation
    if MobileOrientation == "landscape" then
        x, y = love.graphics.inverseTransformPoint(x, y)
    end
    
  if not (touchControls.leftThumbstickX==nil) then
  local distanceToLeftThumbstick = math.sqrt((x - touchControls.leftThumbstickX)^2 + (y - touchControls.leftThumbstickY)^2)
  --local distanceToRightThumbstick = math.sqrt((x - touchControls.rightThumbstickX)^2 + (y - touchControls.rightThumbstickY)^2)

  if distanceToLeftThumbstick <= touchControls.leftThumbstickRadius then
    touchControls.leftThumbstickPressed = false
    touchControls.leftxaxis = 0
    touchControls.leftyaxis = 0
  --elseif distanceToRightThumbstick <= touchControls.rightThumbstickRadius and not (layout=="arcade") then
    --touchControls.rightThumbstickPressed = false
    --touchControls.rightxaxis = 0
    --touchControls.rightyaxis = 0
  else
    touchControls:handleMouseReleased(x, y, button, istouch, presses)
    --TopMenuMousereleased(x, y, button, istouch, presses)
    if NetworkMenuVisible==true then
		InputFieldmousereleased(mx, my, mbutton, pressCount)
	end
  end
  end
  if touchControls.layout=="keyboard" then
		touchKeyboard:mousereleased(x, y, button, istouch, presses)
	end
  
end
