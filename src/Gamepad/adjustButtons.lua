-- Add a new function to adjust the position of the current buttons
function touchControls:adjustButtonPosition(offsetX, offsetY)
  for i, button in ipairs(self.buttons) do
    button.x = button.x + offsetX
    button.y = button.y + offsetY
  end
end
