xboxGamePad = love.graphics.newImage("/src/Gamepad/xboxgamepad.png")
-- Update the existing draw function to draw the right thumbstick and its virtual thumbstick
function touchControls:draw()
    love.graphics.draw(xboxGamePad, self.leftThumbstickX-200, self.leftThumbstickY-200, 0, 0.5, 0.5)

    -- Draw left thumbstick
    love.graphics.circle("line", self.leftThumbstickX, self.leftThumbstickY, self.leftThumbstickRadius)
    love.graphics.circle("line", self.rightThumbstickX, self.rightThumbstickY, self.rightThumbstickRadius)

    -- Draw virtual thumbstick for left thumbstick when pressed and dragged
    if self.leftThumbstickPressed then
        love.graphics.circle("line", self.leftThumbstickX + touchControls.leftxaxis, self.leftThumbstickY + touchControls.leftyaxis, self.leftThumbstickRadius * 1.5)
    end



    -- Draw virtual buttons for left thumbstick (A, B, X, Y)
    for i, button in ipairs(self.buttons) do
 
    if joystick then
     
       -- Check for each button and update their pressed state
      
        --self.button.pressed[1] = joystick:isGamepadDown("a")
        --self.button.pressed[2] = joystick:isGamepadDown("b")
        --self.button.pressed[3] = joystick:isGamepadDown("x")
        --self.button.pressed[4] = joystick:isGamepadDown("y")
       
    end
  
			if i==6 then love.graphics.setColor(1,0,0,1)
		elseif i==2 then love.graphics.setColor(1,0,0,1)
		
		elseif i==3 then love.graphics.setColor(0,0,1,1)
		elseif i==7 then love.graphics.setColor(0,0,1,1)
		
		elseif i==4 then love.graphics.setColor(0,1,0,1)
		elseif i==8 then love.graphics.setColor(0,1,0,1)
		
		elseif i==1 then love.graphics.setColor(0,1,1,1)
		elseif i==5 then love.graphics.setColor(0,1,1,1)
		
		else love.graphics.setColor(1,1,1,1)
		end
    
        -- Update button color based on whether it's pressed
        if button.pressed then
            --love.graphics.setColor(1, 0, 0, 1)  -- Example color for pressed state (red)
            love.graphics.circle("fill", button.x, button.y, self.buttonRadius)
        else
        
            --love.graphics.setColor(1, 1, 1, 1)  -- Default color (white)
            love.graphics.circle("line", button.x, button.y, self.buttonRadius)
        end
    end

    -- Draw right thumbstick if pressed
    if self.rightThumbstickPressed then
        love.graphics.circle("line", self.rightThumbstickX + touchControls.rightxaxis, self.rightThumbstickY + touchControls.rightyaxis, self.rightThumbstickRadius * 1.5)
    end
end

