

-- Register touchpressed event for left and right thumbsticks
function love.touchpressed(id, x, y, dx, dy, pressure)

-- Adjust coordinates based on the current orientation
    
        --x, y = love.graphics.inverseTransformPoint(x, y)
    
    
  local distanceToLeftThumbstick = math.sqrt((x - touchControls.leftThumbstickX)^2 + (y - touchControls.leftThumbstickY)^2)
  --local distanceToRightThumbstick = math.sqrt((x - touchControls.rightThumbstickX)^2 + (y - touchControls.rightThumbstickY)^2)

  if distanceToLeftThumbstick <= touchControls.leftThumbstickRadius then
    touchControls.leftThumbstickPressed = true
    touchControls:updateLeftThumbstickAxes(x, y)
  --elseif distanceToRightThumbstick <= touchControls.rightThumbstickRadius and not (layout=="arcade") then
    --touchControls.rightThumbstickPressed = true
    --touchControls:updateRightThumbstickAxes(x, y)
  else
    touchControls:checkButtonPress(x, y)
    checkMenuButtons(x, y, button, istouch, presses)
    --TopMenuMousepressed(x, y, button, istouch, presses)
    if NetworkMenuVisible==true then
		InputFieldmousepressed(mx, my, mbutton, pressCount)
     end
    
  end
  
  if touchControls.layout=="keyboard" then
    if x==nil then x=0 end
    if y==nil then y=0 end
    touchKeyboard:touchpressed(x, y)
    
    --inputKeyboardTouch(dt)
  end

end
  
-- Register touchmoved event for left and right thumbsticks
function love.touchmoved(id, x, y, dx, dy, pressure)
-- Adjust coordinates based on the current orientation
    
        --x, y = love.graphics.inverseTransformPoint(x, y)
    
  if touchControls.leftThumbstickPressed then
    touchControls:updateLeftThumbstickAxes(x, y)
  --elseif touchControls.rightThumbstickPressed then
    --touchControls:updateRightThumbstickAxes(x, y)
  else
	--TopMenuMousemoved(x, y, dx, dy, istouch)
	 if NetworkMenuVisible==true then
		InputFieldmousemoved(mx, my, dx, dy)
     end
    -- Handle other touchmoved logic
  end
end



-- Register touchreleased event
function love.touchreleased(id, x, y)
-- Adjust coordinates based on the current orientation
    
        --x, y = love.graphics.inverseTransformPoint(x, y)
    
  local distanceToLeftThumbstick = math.sqrt((x - touchControls.leftThumbstickX)^2 + (y - touchControls.leftThumbstickY)^2)
  --local distanceToRightThumbstick = math.sqrt((x - touchControls.rightThumbstickX)^2 + (y - touchControls.rightThumbstickY)^2)

  if distanceToLeftThumbstick <= touchControls.leftThumbstickRadius then
    touchControls.leftThumbstickPressed = false
    touchControls.leftxaxis = 0
    touchControls.leftyaxis = 0
  --elseif distanceToRightThumbstick <= touchControls.rightThumbstickRadius and not (layout=="arcade")  then
    --touchControls.rightThumbstickPressed = false
    --touchControls.rightxaxis = 0
    --touchControls.rightyaxis = 0
  else
    -- If neither thumbstick was touched, handle it as a mouse release
    touchControls:handleMouseReleased(x, y)
    --TopMenuMousereleased(x, y, button, istouch, presses)
    
    	if NetworkMenuVisible==true then
			InputFieldmousereleased(mx, my, mbutton, pressCount)
		end
  end
  if touchControls.layout=="keyboard" then
    touchKeyboard:touchreleased()
  end
   button1or5Pressed=false
  button2or6Pressed=false
    
end
