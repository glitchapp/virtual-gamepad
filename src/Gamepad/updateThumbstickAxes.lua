
-- Update the existing updateThumbstickAxes function to update left thumbstick axes
function touchControls:updateLeftThumbstickAxes(x, y)
  local relativeX = x - self.leftThumbstickX
  local relativeY = y - self.leftThumbstickY
  local length = math.sqrt(relativeX^2 + relativeY^2)
 
 

			self.leftxaxis = relativeX / self.leftThumbstickRadius
			self.leftyaxis = relativeY / self.leftThumbstickRadius

 
 
  
end

-- Add a new function to update right thumbstick axes
function touchControls:updateRightThumbstickAxes(x, y)
  --[[local relativeX = x - self.rightThumbstickX
  local relativeY = y - self.rightThumbstickY
  local length = math.sqrt(relativeX^2 + relativeY^2)

  if length <= self.rightThumbstickRadius then
    self.rightxaxis = relativeX / self.rightThumbstickRadius
    self.rightyaxis = relativeY / self.rightThumbstickRadius
  else
    local scaleFactor = self.rightThumbstickRadius / length
    self.rightxaxis = relativeX * scaleFactor
    self.rightyaxis = relativeY * scaleFactor
  end
  --]]
end
