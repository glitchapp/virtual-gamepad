
-- Modify the removeLeftThumbstickButtons function to remove buttons associated with both thumbsticks
function touchControls:removeLeftThumbstickButtons()
  local buttonsToRemove = {}
  for i, button in ipairs(self.buttons) do
    local distanceToLeftThumbstick = math.sqrt((button.x - self.leftThumbstickX)^2 + (button.y - self.leftThumbstickY)^2)
    local distanceToRightThumbstick = math.sqrt((button.x - self.rightThumbstickX)^2 + (button.y - self.rightThumbstickY)^2)

    -- Check if the button is within the radius of either thumbstick
    if distanceToLeftThumbstick <= self.leftThumbstickRadius + self.buttonRadius or
       distanceToRightThumbstick <= self.rightThumbstickRadius + self.buttonRadius then
      table.insert(buttonsToRemove, i)
    end
  end

  -- Remove buttons associated with both thumbsticks
  for i = #buttonsToRemove, 1, -1 do
    table.remove(self.buttons, buttonsToRemove[i])
  end
end
