function touchControls:createArcadeButtons()
  -- Clear existing buttons
  self:removeLeftThumbstickButtons()
  self:removeRightThumbstickButtons()
  self.buttons = {}

  -- Create buttons around the left thumbstick
				dynamicX  = 0.23		dynamicY  = 1
				dynamicX2 = 0.25		dynamicY2 = 1.05
				dynamicX3 = 0.25		dynamicY3 = 0.95
				dynamicX4 = 0.27		dynamicY4 = 1.0
				

  buttonPositions = {
    {x = love.graphics.getWidth() * dynamicX, y = self.leftThumbstickY * dynamicY},
    {x = love.graphics.getWidth() * dynamicX2, y = self.leftThumbstickY * dynamicY2},
    {x = love.graphics.getWidth() * dynamicX3, y = self.leftThumbstickY * dynamicY3},
    {x = love.graphics.getWidth() * dynamicX4, y = self.leftThumbstickY * dynamicY4},
  }

  -- Create the buttons and add them to the `self.buttons` table
  for _, position in ipairs(buttonPositions) do
    -- Initialize each button with a pressed state (set as false initially)
    table.insert(self.buttons, {x = position.x, y = position.y, pressed = {false, false, false, false}})
  end
  
  -- Optional: Remove any existing buttons associated with the right thumbstick
  -- self:removeRightThumbstickButtons()
end
