function loadVoices()
	connected = love.audio.newSource( "src/assets/voice/connected.ogg","static" )
	searchingForValidInstances = love.audio.newSource( "src/assets/voice/searchingForValidInstances.ogg","static" )
	validInstanceFound = love.audio.newSource( "src/assets/voice/validInstanceFound.ogg","static" )
	noValidInstanceFound = love.audio.newSource( "src/assets/voice/noValidInstanceFound.ogg","static" )
	invalidIp = love.audio.newSource( "src/assets/voice/invalidIp.ogg","static" )
end
