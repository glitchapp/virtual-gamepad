-- Define constants for keyboard dimensions and padding
landscapeKeyboardX = 550
landscapeKeyboardY = 400
portraitKeyboardX = 50
portraitKeyboardY = 700
--local keyWidth = 50
--local keyHeight = 50
local keyPadding = 5

-- Determine the orientation and set keyboard position accordingly
function updateKeyboardPosition()
    local width, height = love.graphics.getDimensions()
    if KeyboardOrientation=="portrait" then -- Landscape orientation
        touchKeyboard.x = portraitKeyboardX
        touchKeyboard.y = portraitKeyboardY
        touchKeyboard.rotation = 0
        keyWidth = 60*1.3
		keyHeight = 60*1.3
		keyPadding = 10*1.3
        
    elseif KeyboardOrientation=="landscape" then
        touchKeyboard.x = landscapeKeyboardX
        touchKeyboard.y = landscapeKeyboardY
        touchKeyboard.rotation = math.pi / 2 -- Rotate 90 degrees
        keyWidth = 60*1.9
		keyHeight = 60*1.9
		keyPadding = 10*1.9
    end
end


function updateKeyboard()

updateKeyboardPosition()
--print(touchControls.layout)
-- Check if the layout is keyboard or mouse
    if touchControls.layout == "keyboard" then
		
        -- Get current touches or mouse click
        local touches = love.touch.getTouches()
        if #touches == 0 and love.mouse.isDown(1) then -- Check if left mouse button is clicked
            -- Get mouse position
            local touchX, touchY = love.mouse.getPosition()
            touchKeyboard:touchpressed(touchX, touchY)
            --print("Mouse Position:", touchX, touchY)
            -- Call touchpressed and touchreleased methods of touchKeyboard or handle mouse input
        else
            -- Iterate over each touch
            for i, touchID in ipairs(touches) do
                -- Get touch position for each touch
                local touchX, touchY = love.touch.getPosition(touchID)
                --print("Touch Position:", touchX, touchY)
                -- Call touchpressed and touchreleased methods of touchKeyboard
            end
        end
        
        -- Call getSelectedKey to retrieve the selected key
       selectedKey = touchKeyboard:getSelectedKey()
        
        if selectedKey then
            --print("Selected Key:", selectedKey)
        end
    end
    --print(selectedKey)
    return selectedKey -- Return selectedKey
end
