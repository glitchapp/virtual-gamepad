

--initialize variables
touchControls.receivedLeftXAxis,touchControls.receivedLeftYAxis=0,0
touchControls.receivedRightXAxis,touchControls.receivedRightYAxis=0,0

-- Function to determine the instance number
local function getInstanceNumber()
  return instanceNumber
end


math.randomseed(os.time()) 
	entity = tostring(math.random(99999))

-- Define variables to store received axis values
receivedLeftXAxis = 0
receivedLeftYAxis = 0
receivedRightXAxis = 0
receivedRightYAxis = 0

function receiveThumbstickAxes()

  -- Attempt to receive data from the UDP socket
  --data, address, port = udp:receivefrom()
  data, receiveAddress, port = udp:receivefrom()
  
  -- Check if data is received
  if data then
  if identifierSent==nil then
	identifierSent=true
	end
  
  
	offsetX,offsetY=200,100
	offsetXright,offsetYright=400,100



    --print("Received data:", data)

    -- Deserialize the received data
    local values = {}
    for value in string.gmatch(data, "([^,]+)") do
      table.insert(values, tonumber(value))
    end


	--debug of data

-- Print each value
for i, value in ipairs(values) do
    print("Value " .. i .. ": " .. value)
		if i==1 then touchControls.receivedLeftXAxis=value 
    elseif i==2 then touchControls.receivedLeftYAxis=value 
    elseif i==3 then touchControls.receivedRightXAxis=value 
    elseif i==4 then touchControls.receivedRightYAxis=value 
    end
end
	
   
	
	print("data received.")
  else
  	offsetX,offsetY=0,0
	offsetXright,offsetYright=0,0
    -- Print a message if no data is received
    --print("No data received.")
  end

end


-- Function to normalize and scale axis values to fit within the range of -1 to 1
function normalizeAxisValue(value)
  -- Assuming the range of received axis values is between 0 and 1,
  -- you can normalize and scale them to fit within the range of -1 to 1
  return (value * 2) - 1
end

