
-- Function to handle keyboard input
function love.keypressed(key)
    -- If the key pressed is "1", set instanceNumber to 1
    if key == "1" then
        instanceNumber = 1
        	receivePort=1024
			sendPort=1025
        print("Instance selected: 1")
    end

    -- If the key pressed is "2", set instanceNumber to 2
    if key == "2" then
        instanceNumber = 2
			receivePort=1025
			sendPort=1024
        print("Instance selected: 2")
    end
end

-- Function to determine the instance number
local function getInstanceNumber()
  return instanceNumber
end



-- Function to send thumbstick axes data
function sendThumbstickAxes(leftxaxis, leftyaxis, rightxaxis, rightyaxis)

  -- Initialize variables if they are nil
  leftxaxis = leftxaxis or 0
  leftyaxis = leftyaxis or 0
  rightxaxis = rightxaxis or 0
  rightyaxis = rightyaxis or 0
  
  
    -- Serialize thumbstick axes
    local data = string.format("%f,%f,%f,%f", leftxaxis, leftyaxis, rightxaxis, rightyaxis)


    -- Send data to the other instance
    udp:sendto(data, sendAddress, sendPort)
    --print("Instance", instanceNumber, "sent data:", data, "to port:", sendPort)


  --print("Data to be sent:", data, "to", sendAddress, "on port", sendPort)
  --print("Sender bound to:", sendAddress, sendPort)
	--local data = "1.680908,7.965088,0.407715,0.000000"
--	print("Data sent:", data)


end

