-- Define variables to keep track of swipe gesture and selected option
local startX, startY = 0, 0
local deltaX, deltaY = 0, 0
isSwiping = false
 menuVisible = false
selectedOption = nil
threshold = 50 -- Threshold to switch between options
local options = {"Option 1", "Option 2", "Option 3", "Option 4", "Option 5", "Option 6", "Option 7"}

-- Variables for scrolling
local scrollOffsetX, scrollOffsetY = 0, 0
local lastMouseX, lastMouseY = 0, 0
local mouseHeld = false
local hasSwiped = false -- Flag to indicate if a swipe has occurred

		Mono64_0xA000_256 = love.graphics.newFont("src/assets/fonts/0xa000/0xA000-Mono.ttf", 256)
		Mono64_0xA000 = love.graphics.newFont("src/assets/fonts/0xa000/0xA000-Mono.ttf", 64)
		Regular12_0xA000 = love.graphics.newFont("src/assets/fonts/0xa000/0xA000-Regular.ttf", 12)
		
		GUI_Sound_Effects_027 = love.audio.newSource("src/assets/GUI-Sounds/GUI_Sound_Effects/GUI_Sound_Effects_027.ogg", "static")
		GUI_Sound_Effects_071 = love.audio.newSource("src/assets/GUI-Sounds/GUI_Sound_Effects/GUISound_Effects_8_BY_Jalastram/GUI_Sound_Effects_071.ogg", "static")
		GUI_Sound_Effects_072 = love.audio.newSource("src/assets/GUI-Sounds/GUI_Sound_Effects/GUISound_Effects_8_BY_Jalastram/GUI Sound Effects_072.ogg", "static")
	

function TopMenuMousepressed(x, y, button, istouch, presses)
	
    mouseHeld = true
    startY = y
    isSwiping = true
    scrollOffsetY = y
    hasSwiped = false -- Reset swipe flag on mouse press
    
end

function TopMenuMousereleased(x, y, button, istouch, presses)

    mouseHeld = false
    isSwiping = false
    menuVisible = false
    
    lastMouseX = 0
    lastMouseY = 0
    if not hasSwiped then
        selectedOption = nil
    end
    deltaX = 0
    deltaY = 0
    
    -- Perform action based on selected option
    if selectedOption == options[1] then 					-- Title Play Map
			 touchControls.layout = "gamepad"
			 touchControls:init()
			 
            GUI_Sound_Effects_072:play()
         print("Option 1 selected: Gamepad layout")   
    elseif selectedOption == options[2] then 				-- Performance mode
			autoFPScheck=false
		        if performanceMode==false then 	performanceMode=true
			elseif performanceMode==true then	performanceMode=false
			end
			
			love.timer.sleep( 0.1 )
        GUI_Sound_Effects_071:play()
        print("Option 2 selected: Arcade layout")   
        
    elseif selectedOption == options[3] then 				-- FPS Graph
    
			if NetworkMenuVisible==true then NetworkMenuVisible=false
		elseif NetworkMenuVisible==false then NetworkMenuVisible=true
		end
			love.timer.sleep( 0.1 )
             
	print("Option 3 selected: Network options")   
    
    elseif selectedOption == options[4] then 				-- Tutorial
        -- Add your action for Option 4 here
				if showAbout==false then showAbout=true
			elseif showAbout==true then	showAbout=false
			end
       
       
    print("Option 4 selected: About")   
    
    elseif selectedOption == options[5] then 				-- Credits
        -- Add your action for Option 5 here
       print("Option 5 selected: Exit")   
		love.event.quit()
		GUI_Sound_Effects_071:play()
    print("Option 5 selected: Exit")   
    
  
    elseif selectedOption == nil then 
        print("No options selected")
    end

end



function TopMenuMousemoved(x, y, dx, dy, istouch)

    if mouseHeld then
        deltaX = x - startX
        deltaY = y - startY

        if MobileOrientation == "portrait" then
            if scrollOffsetY < 600 then
                scrollOffsetY = scrollOffsetY + dy
            end
        elseif MobileOrientation == "landscape" then
            if scrollOffsetX < 600 then
                scrollOffsetX = scrollOffsetX + dx
            end
        end

        local delta = (MobileOrientation == "landscape") and deltaY or deltaX

        for i = 5, 1, -1 do
            if delta > threshold * i then
				
                hasSwiped = true
                selectedOption = options[i]
                menuVisible = true
                GUI_Sound_Effects_027:play()
                return
            end
        end

        selectedOption = nil
        menuVisible = false
        scrollOffsetX = 0
        scrollOffsetY = 0
    end

end




function revealMenu()

    local height, width = love.graphics.getDimensions()
    
    local optionHeight = 50
    local optionSpacing = 50
    local menuWidth = width
    
		 local baseY
    if MobileOrientation == "portrait" then
        baseY = (optionHeight - optionSpacing / 2) + scrollOffsetY - 50
    elseif MobileOrientation == "landscape" then
        baseY = (optionHeight - optionSpacing / 2) - scrollOffsetX + 150
    end
    baseY = baseY or (optionHeight - optionSpacing / 2) - scrollOffsetX + 150
    
     local transparency = 0.7
    love.graphics.setColor(0, 0, 1, transparency) -- Option 1 color (blue)
    love.graphics.rectangle("fill", -200, baseY, menuWidth, optionHeight)
    
    love.graphics.setColor(1, 0, 0, transparency) -- Option 2 color (red)
    love.graphics.rectangle("fill", -200, baseY - optionSpacing, menuWidth, optionHeight)
    
    love.graphics.setColor(0, 1, 0, transparency) -- Option 3 color (green)
    love.graphics.rectangle("fill", -200, baseY - 2 * optionSpacing, menuWidth, optionHeight)
    
    love.graphics.setColor(0, 1, 1, transparency) -- Option 4 color (yellow)
    love.graphics.rectangle("fill", -200, baseY - 3 * optionSpacing, menuWidth, optionHeight)
    
    love.graphics.setColor(0.5, 0.5, 1, transparency) -- Option 5 color
    love.graphics.rectangle("fill", -200, baseY - 4 * optionSpacing, menuWidth, optionHeight)
    --[[
    love.graphics.setColor(0.5, 1, 0.5, transparency) -- Option 6 color
    love.graphics.rectangle("fill", 0, baseY - 5 * optionSpacing, menuWidth, optionHeight)
    
    love.graphics.setColor(1, 0.5, 1, transparency) -- Option 7 color
    love.graphics.rectangle("fill", 0, baseY - 5 * optionSpacing, menuWidth, optionHeight)
    --]]
    love.graphics.setFont(Mono64_0xA000)
    
    for i, option in ipairs(options) do
    
        local optionY = baseY - (i - 1) * optionSpacing
        
        
        
        if selectedOption == option then
            --love.graphics.setColor(0, 1, 0, 1) -- Highlight color (green, semi-transparent)
            local transparency = 1
            love.graphics.rectangle("fill", -200, optionY, menuWidth, optionHeight)
            love.graphics.setColor(1, 1, 1, 1) -- Text color (white)
            
            -- Adjust text based on the option
            local text = ""
				if option == "Option 1" then
					text = "Gamepad layout"
            elseif option == "Option 2" then
                text = "Arcade layout"
            elseif option == "Option 3" then
                text = "Network options"
            elseif option == "Option 4" then
                text = "About"
            elseif option == "Option 5" then
                text = "Exit"
            end
            
            love.graphics.printf(text, width / 16, optionY, 1000, "center", 0, 1)
        end
    end
  
    --love.graphics.setFont(Regular12_0xA000)
end


function hideMenu()

    menuVisible = false
    isSwiping = false

end
