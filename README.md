
# Love-touch-virtual-gamepad

This projects was originally a touch controls tool aimed at porting löve2d games to mobiles. As I'm adding network functions to it, it is being repurposed as a multiplayer server for games.

For now it only supports two players. Players can play locally or over the internet provided that you have the right network setup on your system (ports need to be opened or some VPN tunnel that allows connection).

In order for this server to work, the client side that connects to it should also be implemented.

<img src="screenshot1.avif" width=75% height=75%>


# Set up of network controls

1. Open the menu and select "Network setup"

<img src="screenshot6.avif" width=75% height=75%>

2. Fill in the required fields and follow the instructions on the network setup screen.

If you are testing it over the internet make sure the ports are opened.

On the third screenshot, the two green thumbstick on top of the virtual gamepad represents the remote controls and will move when succesfully connected to another instance which sends the data.

# How to use

To implement this library on any game simply add the following code to main.lua: 
> local touchControls = require("touchControls")

>function love.load()
 > touchControls:init()
>end

>function love.draw()
>  touchControls:draw()
>end

If you want to see an easy and simple implementation of the keyboard layout I suggest you check this [notepad tool] (https://codeberg.org/glitchapp/Lovepad)

# Games using this library

This library is in a preliminary state and is being tested, betatesting and contributions are welcome.

The following games includes a release using the library, the screen resolution and other parameters are tweaked to fit on mobile devices, please check releases for more details:

[Boxclip](https://codeberg.org/glitchapp/Boxclip)  -   2D platformer engine with an emphasis on interactive map editing

[<img src="screenshots/boxclip.webp" width=25% height=25%>](https://codeberg.org/glitchapp/Boxclip)

[Fish fillets remake](https://codeberg.org/glitchapp/fish-fillets-remake) - Remake of the classic puzzle created by Altar interactive

[<img src="screenshots/fmini.webp" width=25% height=25%>](https://codeberg.org/glitchapp/fish-fillets-remake)

[Granite crash](https://codeberg.org/glitchapp/granite-crash) - A little game to collect gems in the spirit of the old "Boulder Dash" game

[<img src="screenshots/granitecrash.webp" width=25% height=25%>](https://codeberg.org/glitchapp/granite-crash)

[Ladybug](https://codeberg.org/glitchapp/love-ladybug) - A LÖVE port of the classic Arcade game Ladybug

[<img src="screenshots/ladybug.webp" width=25% height=25%>](https://codeberg.org/glitchapp/love-ladybug)

[Tetris](https://codeberg.org/glitchapp/LoveTetris)   Tetris clone written in lua, logic from https://simplegametutorials.github.io/love/blocks/

[<img src="screenshots/tetris.webp" width=25% height=25%>](https://codeberg.org/glitchapp/LoveTetris)

[Max-downforce](https://codeberg.org/glitchapp/max-downforce/releases)   arcade racing game in pseudo 3d style and gameplay is similar to the classic Pole Position.

[<img src="screenshots/max.webp" width=25% height=25%>](https://codeberg.org/glitchapp/max-downforce/releases)

[Nova pinball](https://codeberg.org/glitchapp/nova-pinball) -A pinball game created using Nova Pinball Engine

[<img src="screenshots/novapinball.webp" width=25% height=25%>](https://codeberg.org/glitchapp/nova-pinball)

[Pacpac](https://codeberg.org/glitchapp/pacpac)   - a pacman clone written by "tylerneylon".

[<img src="screenshots/pacpac.webp" width=25% height=25%>](https://codeberg.org/glitchapp/pacpac)

[Snak](https://codeberg.org/glitchapp/SNAK)     - Great puzzle game by Justin van der Leij https://keyslam.com/projects/snak/

[<img src="screenshots/snak.webp" width=25% height=25%>](https://codeberg.org/glitchapp/SNAK)

[Starphase](https://codeberg.org/glitchapp/Starphase) - Side scrolling space shooter

[<img src="screenshots/Starphase.webp" width=25% height=25%>](https://codeberg.org/glitchapp/Starphase)

Please be aware that most of the game listed above were not developed by me and I can't maintain / correct bugs related to the games. 

I added to most of them gamepad / thumbstick support and touch controls, I'm grateful for feedback related to the things I added to the forks.

# Credits
InputField - https://github.com/ReFreezed/InputField
Text input program used to set up ip and other variables

# Contributions

If you are a developer and you want to see more games and improve the current ones please check the wiki of this repository.

# License

 Copyright (C) 2024  Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.