local socket = require("socket.http")
local ltn12 = require("ltn12")
local json = require("dkjson")

local inputChannel = love.thread.getChannel("tts_input")
local outputChannel = love.thread.getChannel("tts_output")

while true do
    local request = inputChannel:demand()  -- Wait for a request

    if request and request.text then
        local encodedText = request.text:gsub(" ", "%%20")
        local url = "http://localhost:5002/api/tts?text=" .. encodedText
        url = url .. "&speaker_id=default&language_id="

        local responseBody = {}
        local res, code, headers, status = socket.request {
            url = url,
            method = "GET",
            sink = ltn12.sink.table(responseBody)
        }

        if code == 200 then
            local audioData = table.concat(responseBody)
            local file = io.open("output_audio.wav", "wb")
            file:write(audioData)
            file:close()
            
            -- Send success response
            outputChannel:push({ success = true, audioFile = "output_audio.wav", callback = request.callback })
        else
            -- Send error response
            outputChannel:push({ success = false, error = "TTS request failed" })
        end
    end
end
