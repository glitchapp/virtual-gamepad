local socket = require("socket.http") -- Import LuaSocket
local json = require("dkjson") -- Import dkjson
local channelInput = love.thread.getChannel("input_channel")
local channelOutput = love.thread.getChannel("output_channel")

while true do
    -- Wait for input from the main thread
    local requestBody = channelInput:pop()
    if requestBody then
        local responseBody = {}

        -- Make the HTTP request
        local res, code, headers = socket.request {
            url = requestBody.url,
            method = "POST",
            headers = {
                ["Content-Type"] = "application/json",
                ["Content-Length"] = #requestBody.body
            },
            source = ltn12.source.string(requestBody.body),
            sink = ltn12.sink.table(responseBody)
        }

        if code == 200 then
            -- Decode the JSON response
            local responseJson = table.concat(responseBody)
            local responseData, err = json.decode(responseJson)
            if responseData then
                channelOutput:push({ success = true, response = responseData })
            else
                channelOutput:push({ success = false, error = "JSON decode error: " .. tostring(err) })
            end
        else
            channelOutput:push({ success = false, error = "HTTP request failed with code " .. tostring(code) })
        end
    end
end
